<?php
namespace Basil\Time;

class Index
{
    public static function now()
    {
        return date("Y-m-d H:i:s");
    }
}